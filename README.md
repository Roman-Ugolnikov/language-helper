# README #

This is quite simple porgram for translation of list of words and pharases. 
It could be handy while preparing to the English lessons.

### Sources

This program use :
* [MC Millan Dictionary](http://www.macmillandictionary.com/) as a source for explanation;
* [Яндекс.Словари](https://slovari.yandex.ru/) as a source for translation.

This program does not use any spacial API of the recources. It just visits page of translation as if you do it by your self. 


### Software requirements ###

This program was created using Java platform, thus it does not require using paritcular operation system.
one requirement: you should have Java8 JVM installed on your computer.
you can downloas latest version of java here: http://java.com .

### Concatc info ###

In case of any propositon or improvement you can contact author via email: ugolnikovroman@gmail.com