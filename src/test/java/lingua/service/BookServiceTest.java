package lingua.service;

import java.io.File;
import org.junit.Test;


import static org.junit.Assert.assertTrue;

/**
 * @author r.uholnikov
 */
public class BookServiceTest {

    private BookService bookService = new BookService();

    @Test
    public void getTopUsedWords() throws Exception {
        String path = getClass().getClassLoader().getResource("test_ru.txt").getPath();

        String topUsedWords = bookService.getTopUsedWords(100, new File(path));
        assertTrue("here should be russian words", topUsedWords.startsWith("что"));
    }
}