package lingua.service.yandex.service;

import java.util.Collection;
import org.junit.Test;


import static org.junit.Assert.*;

/**
 * @author r.uholnikov
 */
public class TranslationServiceTest {

    private TranslationService translationService = new TranslationService();

    @Test
    public void getDirections() throws Exception {
        Collection<String> directions = translationService.getDirections();
        System.out.println(directions);
        assertTrue("We should have more then one direction", directions.size()>0);
    }

    @Test
    public void detectLanguage() throws Exception {
        String detectedLanguage = translationService.detectLanguage("русский язык");
        assertEquals("Language has not been detected", "ru", detectedLanguage);
    }

    @Test
    public void getTranslation() throws Exception {
        String translation = translationService.getTranslation("Привет мир", "en").iterator().next();
        assertEquals("Translation is not correct", "Hello world", translation);

    }
}