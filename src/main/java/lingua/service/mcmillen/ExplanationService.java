package lingua.service.mcmillen;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lingua.utils.CacheUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;

import lingua.LanguageProcessor;

/**
 * Created by Roman Ugolnikov on 16-Nov-15.
 */
public class ExplanationService {

    public static final String ENGLISH_DICTIONARY_URL_PATTERN = "http://www.macmillandictionary.com/search/british/direct/?q={0}";


    private static String getSiteText(String url) {
        String result = null;
        HttpURLConnection connection = null;
        try {
            URL siteUrl = new URL(StringUtils.replace(url, " ", "%20"));
            InputStream sin = null;
            connection = (HttpURLConnection) siteUrl.openConnection();
            int statusCode = connection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                siteUrl = new URL(connection.getHeaderField("Location"));
                sin = siteUrl.openStream();
            }
            sin = connection.getInputStream();
            result = IOUtils.toString(sin, "UTF-8");
        } catch (IOException e) {
            //do nothing, misspelling
            //System.out.print("do nothing, misspelling");
        }
        return result;
    }

    /**
     * Get Transcription from remote site. Site is defined here.
     *
     * @param wordToBeExplained
     * @return
     * @throws IOException
     */
    public static String getExplanation(String wordToBeExplained) throws IOException {
        List<String> listOfHtmlTranslations = new ArrayList<String>();
        wordToBeExplained = wordToBeExplained.trim().toLowerCase();
        if (wordToBeExplained.startsWith("to ")) {
            wordToBeExplained = wordToBeExplained.substring(3).replaceAll(" ", "-");
        }
        String resultFromCache = CacheUtils.getExplanation(wordToBeExplained);
        if (resultFromCache != null) {
            return resultFromCache;
        }

        final StringBuilder result = new StringBuilder("");
        String siteContent = getSiteText(MessageFormat.format(ENGLISH_DICTIONARY_URL_PATTERN, wordToBeExplained));
        listOfHtmlTranslations = listOfMatchesMcMillen(siteContent, LanguageProcessor.TRANSLATION_COUNT);
        if (listOfHtmlTranslations.size() < 1) {
            listOfHtmlTranslations = listOfMatches(siteContent, "<span class=\"DEFINITION\" resource=\"[a-zA-Z_-]+\" query=\"[a-zA-Z]+\">(.+)<[a-z-A-Z ]=\"EXAMPLES\"", LanguageProcessor.TRANSLATION_COUNT, 1);
        }
        if (listOfHtmlTranslations.size() < 1) {
            listOfHtmlTranslations = listOfMatches(siteContent, "DEFINITION.+[^>](.*)<div class=\"THES\".*", LanguageProcessor.TRANSLATION_COUNT, 1);
        }
        if (listOfHtmlTranslations.size() > 0) {
            listOfHtmlTranslations.stream().forEach(p -> result.append(Jsoup.parse(p).text()));
        }
        CacheUtils.putExplanation(wordToBeExplained, result.toString());
        return result.toString();
    }

    private static List<String> listOfMatchesMcMillen(String incomingText, int maxTranslation) {
        List<String> stringList = Arrays.asList(StringUtils.splitByWholeSeparator(incomingText, "<span class=\"DEFINITION\""));
        List<String> explanationList = new ArrayList<String>();
        if (stringList.size() == 1) {
            //we did not found any. show "did you mean" section.
            String strWithColon = StringUtils.replace(stringList.get(0), "</li>", "</li>;");
            explanationList.add(StringUtils.substringBefore(StringUtils.substringAfter(strWithColon, "<div id=\"didyoumean\">"), "</ul>"));
            return explanationList;
        }
        if (stringList.size() > 1) {
            stringList = stringList.subList(1, stringList.size());
        }
        int i = 0;
        for (String str : stringList) {
            if (i >= maxTranslation) {
                return explanationList;
            }
            if (StringUtils.contains(str, "<div class=\"EXAMPLES\"")) {
                explanationList.add(StringUtils.substringBefore(StringUtils.substringAfter(str, ">"), "<div class=\"EXAMPLES\""));
            } else {
                explanationList.add(StringUtils.substringBefore(StringUtils.substringAfter(str, ">"), "<div class=\"THES\""));
            }
            i++;
        }
        return explanationList;
    }

    private static List<String> listOfMatches(String str, String regExp, int maxCount, int group) {
        List<String> list = new ArrayList<String>();
        Pattern r = Pattern.compile(regExp);
        Matcher m = r.matcher(str);
        for (int i = 0; (i < maxCount) && (m.find()); i++) {
            list.add(m.group(group));
        }
        return list;
    }


}
