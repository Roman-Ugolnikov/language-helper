package lingua.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author r.uholnikov
 */
public class BookService {

    private Map<String, Integer> usedWords = new HashMap<>();

    /**
     * For now supports only Txt files
     *
     * @param topLimit
     * @param file
     * @return
     */
    public String getTopUsedWords(int topLimit, File file) {
        usedWords.clear();
        try {
            Files.lines(file.toPath()).forEach(this::processCalculationUsageOfWords);
        } catch (IOException e) {
            e.printStackTrace();
        }
        processCalculationUsageOfWords("line");
        Map<String, Integer> mostUsed = new LinkedHashMap<>();
        usedWords.entrySet().stream()
                .filter(entry -> entry.getKey().length() > 2)
                .sorted((entry1, entry2) -> entry2.getValue().compareTo(entry1.getValue()))
                .limit(topLimit)
//                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));
                .forEachOrdered(entry -> mostUsed.put(entry.getKey(), entry.getValue()));

        //separated for testing properties.
        return mostUsed.entrySet().stream()
//                .sorted((entry1, entry2) -> entry2.getValue().compareTo(entry1.getValue()))
                .map(entry -> entry.getKey())
                .reduce((startFileDirUrl, startFileDirUrl2) -> startFileDirUrl + "\n" + startFileDirUrl2)
                .get();
    }

    private void processCalculationUsageOfWords(String line) {
//        String[] words = line.split("[ $&+,:;=?@#|'<>.^*()%!]+");
        String[] words = line.split("[\\p{P}\\p{S} ]+");
        for (String word : words) {
            Integer oldValue = 1;
            if (usedWords.containsKey(word.toLowerCase())) {
                oldValue = usedWords.get(word.toLowerCase());
            }
            usedWords.put(word.toLowerCase(), oldValue + 1);
        }
    }
}
