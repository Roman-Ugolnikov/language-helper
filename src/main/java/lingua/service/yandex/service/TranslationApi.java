package lingua.service.yandex.service;

/**
 * @author r.uholnikov
 */
public class TranslationApi {

    /** token received from yandex */
    public static final String APY_KEY = "trnsl.1.1.20160704T081905Z.09fb3266bb4e501c.7e121557ec3d1620db3a68202eafa715cb477e59";

    private static final String BASE_URL = "https://translate.yandex.net/api/v1.5/tr.json/";
    public static final String AVAILABLE_LANGUAGES = BASE_URL + "getLangs";
    public static final String DETECT_LANGUAGE = BASE_URL + "detect";
    public static final String TRANSLATE_LANGUAGE = BASE_URL + "translate";
}
