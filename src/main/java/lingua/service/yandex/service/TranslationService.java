package lingua.service.yandex.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lingua.service.yandex.domain.responce.DetectedLanguage;
import lingua.service.yandex.domain.responce.Directions;
import lingua.service.yandex.domain.responce.Translation;
import lingua.utils.JsonUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


import static lingua.service.yandex.service.TranslationApi.APY_KEY;
import static lingua.service.yandex.service.TranslationApi.AVAILABLE_LANGUAGES;
import static lingua.service.yandex.service.TranslationApi.DETECT_LANGUAGE;
import static lingua.service.yandex.service.TranslationApi.TRANSLATE_LANGUAGE;

/**
 * @author r.uholnikov
 */
public class TranslationService {

    private RestTemplate restTemplate;
    private Map<String, String> params = new HashMap<>();
    public static Set<String> supportedLanguages = new HashSet<>();

    static{
        supportedLanguages.add("en");
        supportedLanguages.add("es");
        //yandex thinks ua is uk ))
        supportedLanguages.add("uk");
        supportedLanguages.add("ru");
        supportedLanguages.add("fr");
        supportedLanguages.add("de");
        supportedLanguages.add("it");
        supportedLanguages.add("nl");
    }

    public TranslationService() {
        restTemplate = new RestTemplate();
        params.put("key", APY_KEY);
    }

    /**
     * Get available translation directions
     *
     * @return
     */
    public Collection<String> getDirections() {
        String url = buildUrl(AVAILABLE_LANGUAGES, params);
        ResponseEntity<String> entity =
                restTemplate.getForEntity(url, String.class, params);
        Collection<String> dirs = JsonUtil.toJson(entity.getBody(), Directions.class).getDirs();
        List<String> strings = dirs.stream().filter(direction -> {
            String[] split = direction.split("-");
            return supportedLanguages.contains(split[0]) && supportedLanguages.contains(split[1]);
        }).collect(Collectors.toList());

        return strings;
    }

    /**
     * Detects language of text
     *
     * @param text text to to be detected for langugage
     * @return two letters language presentation (like "en")
     */
    public String detectLanguage(String text) {
        params.put("text", text);
        String url = buildUrl(DETECT_LANGUAGE, params);
        ResponseEntity<String> entity =
                restTemplate.getForEntity(url, String.class, params);
        return JsonUtil.toJson(entity.getBody(), DetectedLanguage.class).getLang();
    }

    /**
     * Translates text.
     *
     * @param text      The text to translate.
     * @param direction The translation direction.
     *                  You can set it in either of the following ways:
     *                  As a pair of language codes separated by a hyphen (“from”-“to”). For example, en-ru indicates translating from English to Russian.
     *                  As the target language code (for example, ru). In this case, the service tries to detect the source language automatically.
     * @return {@link Translation} entity
     */
    public Collection<String> getTranslation(String text, String direction) {
        params.put("text", text);
        params.put("lang", direction);
        String url = buildUrl(TRANSLATE_LANGUAGE, params);
        ResponseEntity<String> entity =
                restTemplate.getForEntity(url, String.class, params);
        return JsonUtil.toJson(entity.getBody(), Translation.class).getText();
    }

    private String buildUrl(String baseUrl, Map<String, String> urlParams) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(baseUrl);
        urlParams.entrySet().stream()
                .forEach(entry -> uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue()));
        return uriComponentsBuilder.build().toUriString();
    }
}
