package lingua.service.yandex.domain.responce;

import java.util.Collection;

/**
 * Translation to be received
 *
 * @author r.uholnikov
 */
public class Translation {

    private int code;

    /** value from {@link Directions} */
    private String lang;

    /** translation itself */
    private Collection<String> text;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Collection<String> getText() {
        return text;
    }

    public void setText(Collection<String> text) {
        this.text = text;
    }

}
