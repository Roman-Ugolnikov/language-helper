package lingua.service.yandex.domain.responce;

import java.util.Collection;
import java.util.Map;

/**
 * Translation directions that can be obtained from API
 *
 * @author r.uholnikov
 */
public class Directions {

    private Collection<String> dirs;

    /** key - lang key, value - title */
    private Map<String, String> langs;

    public Collection<String> getDirs() {
        return dirs;
    }

    public void setDirs(Collection<String> dirs) {
        this.dirs = dirs;
    }
}
