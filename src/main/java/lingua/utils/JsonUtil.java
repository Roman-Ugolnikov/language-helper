package lingua.utils;

import com.google.gson.Gson;

/**
 * @author r.uholnikov
 */
public class JsonUtil {

    public static String toJson(Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

    public static <T> T  toJson(String json,  Class<T> classOfT) {
        Gson gson = new Gson();
        return gson.fromJson(json, classOfT);
    }

}
