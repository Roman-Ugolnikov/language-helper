package lingua.utils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


/**
 * Utils for cache control. any logic connected to cache should be there
 *
 * Created by Roman Ugolnikov on 16-Nov-15.
 */
public class CacheUtils {

    private static final ConcurrentMap<String, String> translationCache = new ConcurrentHashMap();
    private static final ConcurrentMap<String, String> transcriptionCache  = new ConcurrentHashMap();
    private static final ConcurrentMap<String, String> explanationCache  = new ConcurrentHashMap();

    public static String getTranslation(String wordToBeTranslated){
        return translationCache.get(wordToBeTranslated);
    }

    /**
     *
     * @param wordToBetranscripted
     * @return
     */
    public static String getTranscription(String wordToBetranscripted){
        return transcriptionCache.get(wordToBetranscripted);
    }

    /**
     *
     * @param wordToBeExplaned
     * @return
     */
    public static String getExplanation(String wordToBeExplaned){
        return explanationCache.get(wordToBeExplaned);
    }

    /**
     *
     * @param key
     * @param value
     */
    public static void putTranslation(String key, String value){
        translationCache.put(key,value);
    }

    /**
     *
     * @param key
     * @param value
     */
    public static void putTranscription(String key, String value){
        transcriptionCache.put(key,value);
    }

    /**
     *
     * @param key
     * @param value
     */
    public static void putExplanation(String key, String value){
        explanationCache.put(key,value);
    }

}
