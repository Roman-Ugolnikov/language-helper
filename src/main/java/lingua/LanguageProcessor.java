package lingua;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import lingua.service.yandex.service.TranslationService;
import lingua.service.mcmillen.ExplanationService;

import javafx.concurrent.Task;


/**
 * class that tries to get translation for entered text
 *
 * @author Roman Uholnikov
 */
public class LanguageProcessor extends Task<String> {

    public static final int TRANSLATION_COUNT = 2;
    private String incomingText;
    private String direction;
    public static Task task;
    private TranslationService translationService;
    private ExplanationService explanationService;

    public LanguageProcessor(final String incomingText, final String direction) {
        this.incomingText = incomingText;
        translationService = new TranslationService();
        this.direction = direction;
    }

    @Override
    public String call() throws IOException {
        StringBuilder stringBuilder = new StringBuilder("");
        List<String> words = Arrays.asList(incomingText.split("\n"));
        updateProgress(0, 1);
        updateMessage("Starting");
        for (int i = 0; i < words.size(); i++) {
            if (words.get(i) == null || words.get(i).length() < 2) {
                continue;
            }
            if (this.isCancelled()) {
                return "canceled";
            }
            String wordTrimmed = words.get(i).trim();
            updateProgress(i, words.size());
            updateMessage(wordTrimmed);
            if(direction.equalsIgnoreCase("en-en")){
                stringBuilder.append(wordTrimmed + " \n\n" + explanationService.getExplanation(wordTrimmed));
            } else {
                if(direction.startsWith("from-") || direction.endsWith("-to")){
                    stringBuilder.append("please, select language");
                } else {
                    stringBuilder.append(wordTrimmed + " -- " + translationService.getTranslation(wordTrimmed, direction));
                }
            }

            stringBuilder.append("\n");
            stringBuilder.append("----------------------------------------------------------------------------\n");
            updateValue(stringBuilder.toString());

        }
        updateProgress(1, 1);
        updateValue(stringBuilder.toString());
        updateMessage("");
        return stringBuilder.toString();
    }


}
