package lingua;


import java.awt.print.PrinterException;
import java.io.File;
import java.text.MessageFormat;
import java.util.List;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.swing.JTextPane;
import lingua.service.yandex.service.TranslationService;
import lingua.service.BookService;

/**
 * @author Roman Uholnikov
 */
public class Main extends Application {

    @FXML
    private Button printButton;

    @FXML
    private Button getTranslationButton;

    @FXML
    private Button buttonFromBook;

    @FXML
    private TextField textFieldTopAmount;

    @FXML
    private Label hintLabel;

    @FXML
    private ComboBox<String> comboBoxDirectionsFrom;

    @FXML
    private ComboBox<String> comboBoxDirectionsTo;

    @FXML
    private TextArea wordsTranslatedArea;

    @FXML
    private TextArea wordsToBeTranslatedArea;

    @FXML
    private ProgressIndicator progressBar;

    private BookService bookService = new BookService();

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("sample.fxml")
        );
        loader.setController(this);
        Parent root = (Parent) loader.load();
        primaryStage.setTitle("Language helper.");
        primaryStage.setScene(new Scene(root, 800, 600));

        comboBoxDirectionsFrom.getItems().addAll(TranslationService.supportedLanguages);
        comboBoxDirectionsTo.getItems().addAll(TranslationService.supportedLanguages);


        printButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                print(wordsTranslatedArea.getText());
            }
        });


        getTranslationButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                getTranslationConcurrently();
            }
        });

        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Txt files only", "*.txt"));
        buttonFromBook.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                        List<File> list =
                                fileChooser.showOpenMultipleDialog(primaryStage);
                        if (list != null) {
                            StringBuilder result = new StringBuilder();
                            wordsToBeTranslatedArea.setText("loading from book");
                            for (File file : list) {
                                String topAmount = textFieldTopAmount.getText();

                                result.append(bookService.getTopUsedWords(Integer.valueOf(topAmount),file));
                            }
                            wordsToBeTranslatedArea.setText(result.toString());
                        } else {
                            wordsToBeTranslatedArea.setText("Book is not selected. Click 'from book' button");
                        }
                    }
                });

        primaryStage.getIcons().add(new Image("https://bitbucket.org/Roman-Ugolnikov/language-helper/downloads/piledbooks.png"));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }


    public void getTranslationConcurrently() {
        progressBar.progressProperty().unbind();
        wordsTranslatedArea.textProperty().unbind();
        hintLabel.textProperty().unbind();
        if (LanguageProcessor.task != null && LanguageProcessor.task.isRunning()) {
            LanguageProcessor.task.cancel(true);
            progressBar.setProgress(0);
            wordsTranslatedArea.clear();
            hintLabel.setText("");
        }

        String direction = MessageFormat.format("{0}-{1}",
                comboBoxDirectionsFrom.getSelectionModel().getSelectedItem(),
                comboBoxDirectionsTo.getSelectionModel().getSelectedItem());
        LanguageProcessor.task = new LanguageProcessor(wordsToBeTranslatedArea.getText(),
                direction);
        progressBar.progressProperty().bind(LanguageProcessor.task.progressProperty());
        wordsTranslatedArea.textProperty().bind(LanguageProcessor.task.valueProperty());
        hintLabel.textProperty().bind(LanguageProcessor.task.messageProperty());

        Thread thread = new Thread(LanguageProcessor.task);
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * @param str The string  to be printed.
     */
    public void print(String str) {

        JTextPane textPane = new JTextPane();

        textPane.setText(str);

        try {
            textPane.print();
        } catch (PrinterException e) {
            //e.printStackTrace();
            System.out.println("problem with printing " + e.getMessage());
        }
    }

}
